
<div align="center">
<br/>

  <h1 align="center">
    Pear Admin Ant
  </h1>
  <h4 align="center">
    开 箱 即 用 的 Vue 3.0 前 端 开 发 框 架
  </h4> 

  [预 览](http://ant.pearadmin.com)   |   [官 网](http://www.pearadmin.com/)   |   [群聊](https://jq.qq.com/?_wv=1027&k=5OdSmve)   |   [社区](http://forum.pearadmin.com/)

</div>

<p align="center">
    <a href="#">
        <img src="https://img.shields.io/badge/Pear Admin Ant-0.0.1+-green.svg" alt="Pear Admin Ant Version">
    </a>
    <a href="#">
        <img src="https://img.shields.io/badge/Vue-3.0.0+-green.svg" alt="Vue Version">
    </a>
      <a href="#">
        <img src="https://img.shields.io/badge/Ant Design Vue-2.0.0.beta+-green.svg" alt="Ant Design Vue Version">
    </a>
</p>
<div align="center">
  <img  width="92%" style="border-radius:10px;margin-top:20px;margin-bottom:20px;box-shadow: 2px 0 6px gray;" src="https://images.gitee.com/uploads/images/2020/1028/001252_3d381589_4835367.png" />
</div>

### 开源地址

Gitee 开源地址: [前往下载](https://gitee.com/Jmysy/pear-admin-ant)


Github 开源地址: [前往下载](https://github.com/Jmysy/Pear-Admin-Ant)

### 项目安装
```
npm install
```

### 项目运行
```
npm run serve
```

### 编译项目
```
npm run build
```

## 示例截图

![输入图片说明](https://images.gitee.com/uploads/images/2020/1028/001252_3d381589_4835367.png "演示地址.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1028/000904_3cd48868_4835367.png "预览界面.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214245_00adb66f_4835367.png "ant1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214254_4677557c_4835367.png "ant2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214302_b36f13d8_4835367.png "ant3.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214310_a363ecb3_4835367.png "ant4.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214319_5e551a1b_4835367.png "ant5.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214328_b50b62e8_4835367.png "ant6.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214337_e99d7b78_4835367.png "ant7.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214346_4e5261c2_4835367.png "ant8.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214354_d3c1d47d_4835367.png "ant9.png")